package com.candidjava;

import com.bank.beans.Client;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginController
 */
public class LoginController extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String un = request.getParameter("username");
        String pw = request.getParameter("password");

        // Connect to mysql and verify username password

        try {
            Class.forName("com.mysql.jdbc.Driver");
            // loads driver
            Connection c = DriverManager.getConnection("jdbc:mysql://localhost:3306/bank", "root", "123456"); // gets a new connection

            PreparedStatement ps = c.prepareStatement("select email,pwd from employee where email=? and pwd=?");
            ps.setString(1, un);
            ps.setString(2, pw);

            ResultSet rs = ps.executeQuery();

            if( rs.next() )
            {
                PreparedStatement ps2 = c.prepareStatement("select * from client");
                ResultSet rs2 = ps2.executeQuery();
                List<Client> listClient = new ArrayList<Client>();
                while (rs2.next()) {
                    Client _client = new Client();

                    _client
                            .setAddress(rs2.getString("address"))
                            .setName(rs2.getString("name"))
                            .setFirstname(rs2.getString("firstname"));

                    listClient.add((_client));

                }

                request.setAttribute("result", listClient);
                response.sendRedirect("success.jsp");
                return;

            }

            response.sendRedirect("error.html");
            return;

        } catch (ClassNotFoundException | SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{


    }

}

