package com.bank.servlet;

import com.bank.beans.Database;
import com.bank.beans.Client;


import javax.servlet.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import com.bank.beans.Client;


    @WebServlet(name = "Servlet",
            urlPatterns = "/liste")
    public class ServletClient extends javax.servlet.http.HttpServlet {
        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        }


        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
            try {
                Connection connection = Database.connect();
                Statement statement = connection.createStatement();
                ResultSet data =statement.executeQuery("SELECT * FROM client");
                List<Client> result = new ArrayList<>();

                while(data.next()){
                    Client client = new Client();

                    client.setIdClient(data.getInt("id"));
                    client.setName(data.getString("nom"));
                    client.setFirstname(data.getString("prenom"));
                    client.setBirthday(data.getDate("dateDeNaissance"));
                    client.setAddress(data.getString("adresse"));

                    result.add(client);

                }
                request.setAttribute("result",result);

            }catch (Exception e){
                e.printStackTrace();
            }
            RequestDispatcher View = request.getRequestDispatcher("success.jsp");
            View.forward(request,response);
        }
    }



