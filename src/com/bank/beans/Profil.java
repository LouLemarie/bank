package com.bank.beans;

public class Profil {

    private int idProfil;

    private int ageMin;

    private int ageMax;

    private int incomeMin;

    private boolean havingChild;

    private boolean student;


    public int getIdProfil() {
        return idProfil;
    }

    public void setIdProfil(int idProfil) {
        this.idProfil = idProfil;
    }

    public int getAgeMin() {
        return ageMin;
    }

    public void setAgeMin(int ageMin) {
        this.ageMin = ageMin;
    }

    public int getAgeMax() {
        return ageMax;
    }

    public void setAgeMax(int ageMax) {
        this.ageMax = ageMax;
    }

    public int getIncomeMin() {
        return incomeMin;
    }

    public void setIncomeMin(int incomeMin) {
        this.incomeMin = incomeMin;
    }


    public boolean isHavingChild() {
        return havingChild;
    }

    public void setHavingChild(boolean havingChild) {
        this.havingChild = havingChild;
    }

    public boolean isStudent() {
        return student;
    }

    public void setStudent(boolean student) {
        this.student = student;
    }


}
