package com.bank.beans;

public class Profession {


        private int idProfession;

        private String profession;



        public int getIdProfession() {
            return idProfession;
        }

        public void setIdProfession(int idProfession) {
            this.idProfession = idProfession;
        }

        public String getProfession() {
            return profession;
        }

        public void setProfession(String profession) {
            this.profession = profession;
        }
    }

