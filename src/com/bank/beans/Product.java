package com.bank.beans;

import java.sql.Date;

public class Product {





    private int idProduct;

    private String label;

    private boolean exclusive;

    private Date limited;

    private int taux;

    private int profil_idProfil;

    private int category_product_idCategory_product;









    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


    public boolean isExclusive() {
        return exclusive;
    }

    public void setExclusive(boolean exclusive) {
        this.exclusive = exclusive;
    }

    public Date getLimited() {
        return limited;
    }

    public void setLimited(Date limited) {
        this.limited = limited;
    }


    public int getTaux() {
        return taux;
    }

    public void setTaux(int taux) {
        this.taux = taux;
    }

    public int getProfil_idProfil() {
        return profil_idProfil;
    }

    public void setProfil_idProfil(int profil_idProfil) {
        this.profil_idProfil = profil_idProfil;
    }

    public int getCategory_product_idCategory_product() {
        return category_product_idCategory_product;
    }

    public void setCategory_product_idCategory_product(int category_product_idCategory_product) {
        this.category_product_idCategory_product = category_product_idCategory_product;
    }
}
