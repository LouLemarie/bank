package com.bank.beans;

import java.sql.Date;

public class Product_has_Client {


    private int product_idProduct;

    private int client_idClient;

    private Date date;





    public int getProduct_idProduct() {
        return product_idProduct;
    }

    public void setProduct_idProduct(int product_idProduct) {
        this.product_idProduct = product_idProduct;
    }

    public int getClient_idClient() {
        return client_idClient;
    }

    public void setClient_idClient(int client_idClient) {
        this.client_idClient = client_idClient;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
