package com.bank.beans;

import java.sql.Date;


public class Client {




    private int idClient;

    private String name;

    private String firstname;

    private String address;

    private Date birthday;

    private int monthly_income;

    private int reference;

    private boolean advertising;

    private boolean havingChild;

    private int profession_idProfession;

    private int situation_idSituation;

    private String email;

    private int phone;








    public int getIdClient() {
        return idClient;
    }

    public Client setIdClient(int idClient) {
        this.idClient = idClient;return this;
    }

    public String getName() {
        return name;
    }

    public Client setName(String name) {
        this.name = name; return this;
    }

    public String getFirstname() {
        return firstname;
    }

    public Client setFirstname(String firstname) {
        this.firstname = firstname; return this;
    }

    public String getAddress() {
        return address;
    }

    public Client setAddress(String address) {
        this.address = address; return this;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getMonthly_income() {
        return monthly_income;
    }

    public void setMonthly_income(int monthly_income) {
        this.monthly_income = monthly_income;
    }

    public int getReference() {
        return reference;
    }

    public void setReference(int reference) {
        this.reference = reference;
    }

    public boolean isAdvertising() {
        return advertising;
    }

    public void setAdvertising(boolean advertising) {
        this.advertising = advertising;
    }

    public boolean isHavingChild() {
        return havingChild;
    }

    public void setHavingChild(boolean havingChild) {
        this.havingChild = havingChild;
    }

    public int getProfession_idProfession() {
        return profession_idProfession;
    }

    public void setProfession_idProfession(int profession_idProfession) {
        this.profession_idProfession = profession_idProfession;
    }

    public int getSituation_idSituation() {
        return situation_idSituation;
    }

    public void setSituation_idSituation(int situation_idSituation) {
        this.situation_idSituation = situation_idSituation;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }
}

