package com.bank.beans;

import java.sql.Date;

public class Advertising {




    private int idAdvertising;

    private boolean match;

    private boolean offers;

    private Date date_offers;

    private boolean accept;

    private int client_idClient;

    private int product_idProduct;







    public int getIdAdvertising() {
        return idAdvertising;
    }

    public void setIdAdvertising(int idAdvertising) {
        this.idAdvertising = idAdvertising;
    }

    public boolean isMatch() {
        return match;
    }

    public void setMatch(boolean match) {
        this.match = match;
    }

    public boolean isOffers() {
        return offers;
    }

    public void setOffers(boolean offers) {
        this.offers = offers;
    }

    public Date getDate_offers() {
        return date_offers;
    }

    public void setDate_offers(Date date_offers) {
        this.date_offers = date_offers;
    }

    public boolean isAccept() {
        return accept;
    }

    public void setAccept(boolean accept) {
        this.accept = accept;
    }

    public int getClient_idClient() {
        return client_idClient;
    }

    public void setClient_idClient(int client_idClient) {
        this.client_idClient = client_idClient;
    }

    public int getProduct_idProduct() {
        return product_idProduct;
    }

    public void setProduct_idProduct(int product_idProduct) {
        this.product_idProduct = product_idProduct;
    }
}
