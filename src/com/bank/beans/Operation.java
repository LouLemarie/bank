package com.bank.beans;

import java.sql.Date;

public class Operation {



    private int idOperation;

    private int balance;

    private Date date;

    private int account_idAccount;

    private int categorie_operation_idCategotie_operation;






    public int getIdOperation() {
        return idOperation;
    }

    public void setIdOperation(int idOperation) {
        this.idOperation = idOperation;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getAccount_idAccount() {
        return account_idAccount;
    }

    public void setAccount_idAccount(int account_idAccount) {
        this.account_idAccount = account_idAccount;
    }

    public int getCategorie_operation_idCategotie_operation() {
        return categorie_operation_idCategotie_operation;
    }

    public void setCategorie_operation_idCategotie_operation(int categorie_operation_idCategotie_operation) {
        this.categorie_operation_idCategotie_operation = categorie_operation_idCategotie_operation;
    }
}
