package com.bank.beans;

public class CategorieProduct {






    private int idCategorie_product;

    private String categorie;

    private int idParent;

    private int depth;






    public int getIdCategorie_product() {
        return idCategorie_product;
    }

    public void setIdCategorie_product(int idCategorie_product) {
        this.idCategorie_product = idCategorie_product;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public int getIdParent() {
        return idParent;
    }

    public void setIdParent(int idParent) {
        this.idParent = idParent;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }
}
